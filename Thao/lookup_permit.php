<?php include_once('header.php')?>
<?php if( !isset($_COOKIE['curruid']))
      echo '<script type="text/javascript">
           window.location = "./index.php"
      </script>'; /* if not logged in return to index */
?>
<?php if( !$currurecord['permissions'] == 'staff')
      echo '<script type="text/javascript">
           window.location = "./dashboard.php"
      </script>'; /* if not staff return to dashboard */
?>

    <div class="container">

      <div class="row">
        <div class="col-md-6">

          <br>
            <h1> Find Permit Details </h1>
          <br>
           <div class="col-lg-16">
           <form action="./lookup_permit.php" method="get">
                <input type="text" name="permitNum" value="<?php echo isset($_GET['pid'])? $_GET['pid']: '' ?>" class="form-control" placeholder="Permit Number"><br>
                <input type="text" name="vehicleNum" value="<?php echo isset($_GET['liscencenumber'])? $_GET['liscencenumber']: '' ?>" class="form-control" placeholder="Liscence Plate Number"><br>
                <input type="text" name="vehicleType" value="<?php echo isset($_GET['vehicletype'])? $_GET['vehicletype']: ''  ?>" class="form-control" placeholder="Vehicle Type"><br>
                <input type="text" name="department" value="<?php echo isset($_GET['department'])? $_GET['department']: '' ?>" class="form-control" placeholder="Department"><br>

                <button class="btn btn-default" type="submit" name="submit">Search</button>
            </form>
            </div> <!-- /.col-lg-16 -->
          </div> <!-- /.row -->
        </div> <!-- /container -->

          <hr>

          <div class="container">

            <?php if (isset($_GET['submit'])) include_once('lookup_permit_result.php'); ?>

              <p><a class="btn btn-default" href="dashboard.php" role="button">&laquo; Back</a></p>
        </div> <!-- /container -->
