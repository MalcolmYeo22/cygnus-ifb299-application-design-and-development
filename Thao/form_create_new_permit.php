<?php include_once('header.php');
      require_once('../connect_mysql.php');

if( !isset($_COOKIE['curruid']))
  echo '<script type="text/javascript">
       window.location = "./index.php"
  </script>';

?>

<div class="container">
 <br>
 <p><a class="btn btn-default" href="./index.php" role="button"> &laquo; Home</a></p>
<p>
<h2>Create new permit</h2>
<p>Please fill out the form provided. Fields with a * are mandatory. <br> If you need help or have questions please contact our Help desk: <b>000-111-222-333</b> <br>or email: <b>helpdesk@atmiyacollege.edu.au</b></p>

</p>
<form action="./process_create_new_permit.php" method="post">
<p>

<?php

// if (isset($_POST['startDate'])) {
//   $startDate = $_POST['startDate'];
//   if (strtotime($startDate) <= time()) {
//     echo "Please select date after the current date";
//     echo $startDate;
//   } else {
//     $startDate = new DateTime($startDate);
//     // $globals['endDate'] = $startDate->modify('+2 day');
//     $endDate = 13;
//   }
// }
  //
  // $startDate = $_GET['startDate'];
  // $startDate = new DateTime($startDate);
  // $endDate = $startDate->modify('+2 day');

  // // if solved with grabbing start date, i think this should work
  // $sel_opt = $_POST['duration'];
  // // $startDate = $_POST['startDate'];
  // // $startDate = new DateTime($startDate);
  // $endDate = new DateTime('01-02-2016');
  //
  // switch($sel_opt) {
  //   case 'Hourly':
  //     $endDate = $endDate->modify('+0 day');
  //     break;
  //   case 'Daily':
  //     $endDate = $endDate->modify('+0 day');
  //     break;
  //   case 'Monthly':
  //     $endDate = $endDate->modify('+1 month');
  //     break;
  //   case 'Yearly':
  //     $endDate = $endDate->modify('+1 year');
  //     break;
  //   default:
  //     echo "something with duration goes wrong";
  // }

  // Test purpose, work properly --> problem with grabbling start date and other else (?)
  // $endDate = new DateTime('01-02-2016');
  // $endDate = $endDate->modify('+1 year');


// require_once('../connect_mysql.php');
//
// $result1 = mysqli_query($con,"select * from users")
//    or die("failed to get data from user table: ".mysqli_error($con));
//
// while ($record1 = mysqli_fetch_array($result1)) {
//   $customerType = $record1['permissions'];
// }

$customerType = $currurecord['permissions'];

$result2 = mysqli_query($con,"select * from prices")
   or die("failed to get data from price table: ".mysqli_error($con));

while ($record2 = mysqli_fetch_array($result2)) {
  $price = $record2['priceper'];
}

?>

<table width="65%" style="border:0px;">
  <tr>
    <td colspan="2" style="border:0px;">
      <br>Customer type: <br>
      <input  class="btn btn-default"  type = "text" name = "CustomerType" size="18" value="<?php echo $customerType; ?>" disabled=""/>
      <br>
    </td>
  </tr>

  <tr>
    <td colspan="2" style="border:0px;">
      <br>Department faculty*: <br>
      <select name="department" id="department" style="width:180px" required>
        <option values="Law">Law</option>
        <option values="Math">Maths</option>
        <option values="Science">Science</option>
        <option values="Engineering">Engineering</option>
        <option values="Admin">Admin</option>
      <select>
      <br>
    </td>
  </tr>

  <tr>
    <td colspan="2" style="border:0px;">
      <br>License plate number*: <br>
      <input  class="btn btn-default"  type = "text" name = "licensePlate" size="18" placeholder = 'eg. 878GTC' required pattern="[a-z0-9]{0,12}" title="License plate number is restricted from 0 to 10 characters"/>
      <br>

    </td>
  </tr>

  <tr>
    <td colspan="2" style="border:0px;">
      <br>Vehicle type*: <br>
      <select name="vehicleType" id="vehicleType" style="width:180px">
        <option values="2wheel">2wheel</option>
        <option values="4wheel">4wheel</option>
        <option values="other">other</option>
      <select>
      <br>
    </td>
  </tr>

  <tr>
    <td width="30%" style="border:0px;">
      <br>Duration*: <br>
      <select name="duration" id="duration" style="width:180px">
        <option values="Hourly">Hourly</option>
        <option values="Daily">Daily</option>
        <option values="Monthly">Monthly</option>
        <option values="Yearly">Yearly</option>
      <select>
      <br>
      <br>Start date*: <br>
      <input type="date" name="startDate" size="19" placeholder="DD-MM-YYYY" required pattern="[0-3][0-9]-[0-1][0-9]-[0-9]{4}" title="Date format is DD-MM-YYYY with valid "/>

    </td>

    <!-- <?php
    $sel_opt = $_POST['duration'];
    // $startDate = $_POST['startDate'];
    // $startDate = new DateTime($startDate);
    $endDate = new DateTime('01-02-2016');

    switch($sel_opt) {
      case 'Hourly':
        $endDate = $endDate->modify('+0 day');
        break;
      case 'Daily':
        $endDate = $endDate->modify('+0 day');
        break;
      case 'Monthly':
        $endDate = $endDate->modify('+1 month');
        break;
      case 'Yearly':
        $endDate = $endDate->modify('+1 year');
        break;
      default:
        echo "something with duration goes wrong";
    }
    ?> -->

    <td width="30%" style="border:0px;">
      <br>Hours*: <br>
      <select name="hours" id="hours"  style="width:180px">
        <option values="1 Hour"> 1 Hour </option>
        <option values="2 Hours"> 2 Hours </option>
        <option values="3 Hours"> 3 Hours </option>
        <option values="4 Hours"> 4 Hours </option>
        <option values="5 Hours"> 5 Hours </option>
      <select>
      <br>
      <br>End date: <br>
      <input  class="btn btn-default" type="date" name="endDate" size="18"  value=<?php echo date_format( $endDate, 'd-m-Y'); ?> disabled=""/>
      <br>
    </td>
  <tr>

  <tr>
    <td colspan="2" style="border:0px;">
      <br>Price: <br>
      <input  class="btn btn-default"  type = "text" name = "price" size="18" value=<?php echo $price; ?> disabled=""/>
      <br>
    </td>
  </tr>

  <tr>
    <td colspan="2" style="border:0px;">
      <br>
      <br>
      <p><input class="btn btn-primary btn-lg"  type="checkbox" required name="Rules"> I'm aware of the <u>Atmiya College Health and Safety Parking Rules</u></p>
    </td>
  </tr>

  <tr>
    <td colspan="2" style="border:0px;">
      <br>
      <br>
      <input class="btn btn-primary btn-lg"  type="submit" name="createP" value="Create a Permit &raquo;" />
    </td>
  </tr>
<table>

</p>

</form>
</div class>

<?php include_once('footer.php')?>
