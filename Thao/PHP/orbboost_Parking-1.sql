-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 12, 2016 at 12:06 AM
-- Server version: 5.5.48-37.8
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `orbboost_Parking`
--

-- --------------------------------------------------------

--
-- Table structure for table `Permits`
--

CREATE TABLE IF NOT EXISTS `Permits` (
  `PermitID` int(10) NOT NULL,
  `UserID` int(10) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `LiscencePlateNumber` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Staff` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `UserID` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `Password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `FirstName` text COLLATE utf8_unicode_ci NOT NULL,
  `LastName` text COLLATE utf8_unicode_ci NOT NULL,
  `Address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MobileNumber` int(10) NOT NULL,
  `Email` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `Permissions` int(1) NOT NULL,
  `OverdueFines` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`UserID`, `Password`, `FirstName`, `LastName`, `Address`, `MobileNumber`, `Email`, `Permissions`, `OverdueFines`) VALUES
('s0000022', 'password', 'Greg', 'Gregson', '123 Gregshouse Lane Brisbane 5000 QLD', 433443434, 'greg.gregson@atmiyacollege.edu.au', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Permits`
--
ALTER TABLE `Permits`
  ADD PRIMARY KEY (`PermitID`), ADD KEY `UserID` (`UserID`), ADD KEY `PermitID` (`PermitID`), ADD KEY `PermitID_2` (`PermitID`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`UserID`), ADD KEY `UserID` (`UserID`), ADD KEY `UserID_2` (`UserID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
