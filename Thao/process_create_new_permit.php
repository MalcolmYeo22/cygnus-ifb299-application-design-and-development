<?php if( !isset($_COOKIE['curruid']))

      echo '<script type="text/javascript">
           window.location = "./index.php"
      </script>';

  $herefromform = isset($_POST['createP']);
  if ($herefromform) {

  #var_dump($_POST);

    $deparment = $_POST['department'];
    $licensePlate = $_POST['licensePlate'];
    $vehicleType = $_POST['vehicleType'];
    $duration = $_POST['duration'];
    $hours = $_POST['hours'];
    $startDate = $_POST['startDate'];
    $endDate = $_POST['endDate'];
    $price = $_POST['price'];

    // create a new email and send to the user with permit details
    require_once('../connect_mysql.php');

?>

<?php include_once('header.php')?>

     <div class="jumbotron">
      <div class="container">

            <p><a class="btn btn-default" href="./index.php" role="button"> &laquo; Home</a></p>
            <br>

           <?php
           echo "<h3>Permit Detail Summary: </h3><br>Department: ".$deparment."
           <br>License Plate number: ".$licensePlate."
           <br> Vehicle Type: ".$vehicleType."
           <br> Duration: ".$duration."
           <br> Start Date: ".$startDate."
           <br> End Date: ".$endDate."
           <br> Prices: ".$price."<br>"
           ;

           ;
           $firstname = $currurecord['firstname'];
           $uid  = $currurecord['uid'];

           // Upload to database
           $query = "insert into permits (pid, uid, duration, startdate, enddate, liscencenumber,
           vehicletype, department) values (NULL, $uid, ?, ?, ?, ?, ?, ?)";
           $prepared_stmt = mysqli_prepare($con, $query);
           mysqli_stmt_bind_param($prepared_stmt,"ssssss", $duration, $startDate, $endDate, $licensePlate, $vehicleType, $deparment);
           //here "issssiss" is the datatypes: (int, string, string...)
           mysqli_stmt_execute($prepared_stmt);
           $affected_rows = mysqli_stmt_affected_rows($prepared_stmt);
           if($affected_rows == 1){
            echo '<br> Create a permit completed correctly.';
           }
           else{
            echo 'Database error; couldn\'t create new permit.';
           }
           mysqli_stmt_close($prepared_stmt);
         }
         mysqli_close($con);

         // send automated email
         include('send_permit_email.php');
         ?>

      </div>
      </div>

 <?php include_once('footer.php')?>
