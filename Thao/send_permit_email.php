<?php
  require_once('PHPMailer-master/class.phpmailer.php');

  // Get pid of the permit
  // $con,"select * from permits, users where users.uid = permits.uid and pid like '%". $_GET['permitNum'] ."%'"

  // $result = mysqli_query($con,"select pid from permits where liscencenumber like '%". $licensePlate ."%' and startdate like '%". $startDate ."%'")
  //   or die("failed to get data from permits table: ".mysqli_error($con));
  //
  // $record = mysqli_fetch_array($result);
  // $permit_id = $record['pid'];

  // Automated send email
  $email = new PHPMailer();
  $email->From = 'atmiyaCollegeParking@hotmail.com';
  $email->FromName = 'Atmiya College';
  $email->Subject = 'Parking permit details';

  // html body message
  $body = "</pre>
  <div>";
  $body .= "<br> Hello  $firstname </br>";
  $body .= "<p> Your parking permit details has been recorded in our database. The parking rules is attached with this email.
  </p>";
  $body .= "<table border='1' width='30%'>";
  // $body .= "<p>Permit ID: $permit_id.";
  $body .= "<tr align='center'><br><p>Department: $deparment.";
  $body .= "<br>License Plate number: $licensePlate";
  $body .= "<br> Vehicle Type: $vehicleType";
  $body .= "<br> Duration: $duration";
  $body .= "<br> Start Date: $startDate";
  $body .= "<br> End Date: $endDate";
  $body .= "<br> Prices: $price<br></br><p>";
  $body .= "</tr></table>";

  $body .= "<p><br> This is your parking permit ticket. Please print and attach to your vehicle.";
  $body .= "<br><br>Sincerely</br>";
  $body .= "<br>Atmiya College</br></p>";
  $body .= "</div>";

  //$email->AddAddress($currurecord['email']);
  $email_address = $currurecord['email'];
  $email->AddAddress($email_address);
  $email->MsgHTML($body);
  // $file_to_attach = '/Atmiya College Health and Safety Parking Rules.pdf';
  //$email->AddAttachment($file_to_attach, 'Atmiya College Health and Safety Parking Rules.pdf');
  //$email->AddAttachment('./Atmiya College Health and Safety Parking Rules.pdf');

  if(!$email->AddAttachment('./Atmiya_College_Health_and_Safety_Parking_Rules.pdf')) {
  //if(!$email->AddAttachment('/home1/orbboost/public_html/Cygnus/atmiya/Atmiya College Health and Safety Parking Rules.pdf')) {
    echo 'Attachment could not be included.';
    echo 'Mailer Error: ' .$email->ErrorInfo;
  } else {
    echo 'A parking rules attachment has been included. ';
  }


  if(!$email->Send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' .$email->ErrorInfo;
  } else {
    echo 'A confirmation of your permit has been sent to your email. Please read the parking rules carefully to prevent violations.';
  }

?>
