-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Users` (
  `UserID` INT NULL,
  `Password` VARCHAR(17) NULL,
  `FirstName` TEXT(12) NULL,
  `Address` VARCHAR(50) NULL,
  `MobileNumber` INT NULL,
  `Email` VARCHAR(45) NULL,
  `Permissions` INT(1) NULL,
  `OverdueFines` TINYINT(1) NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE INDEX `ID_UNIQUE` (`UserID` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Permits`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Permits` (
  `PermidID` INT NULL,
  `UserID` INT NOT NULL,
  `StartDate` DATETIME NULL,
  `EndDate` DATETIME NULL,
  `LicensePlateNumber` VARCHAR(12) NULL,
  `Staff` TINYINT(1) NULL,
  PRIMARY KEY (`PermidID`),
  UNIQUE INDEX `PermidID_UNIQUE` (`PermidID` ASC),
  INDEX `UserID` (`UserID` ASC),
  CONSTRAINT `UserID`
    FOREIGN KEY (`UserID`)
    REFERENCES `mydb`.`Users` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Violations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Violations` (
  `ViolationID` INT NOT NULL,
  `UserID` INT NOT NULL,
  `StartDate` DATETIME NULL,
  `Description` VARCHAR(100) NULL,
  `Status` VARCHAR(45) NULL,
  PRIMARY KEY (`ViolationID`, `UserID`),
  INDEX `UserID` (),
  INDEX `UserID_idx` (`UserID` ASC),
  CONSTRAINT `UserID`
    FOREIGN KEY (`UserID`)
    REFERENCES `mydb`.`Users` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Tickets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Tickets` (
  `TicketID` INT NULL,
  `UserID` INT NULL,
  `StartDate` DATETIME NULL,
  `EndDate` DATETIME NULL,
  `ViolationID` INT NOT NULL,
  PRIMARY KEY (`TicketID`, `ViolationID`),
  UNIQUE INDEX `TicketID_UNIQUE` (`TicketID` ASC),
  INDEX `ViolationID` (),
  INDEX `ViolationID_idx` (`ViolationID` ASC),
  INDEX `UserID` (),
  INDEX `UserID_idx` (`UserID` ASC),
  CONSTRAINT `ViolationID`
    FOREIGN KEY (`ViolationID`)
    REFERENCES `mydb`.`Violations` (`ViolationID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `UserID`
    FOREIGN KEY (`UserID`)
    REFERENCES `mydb`.`Users` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `mydb` ;

-- -----------------------------------------------------
--  routine1
-- -----------------------------------------------------

DELIMITER $$
USE `mydb`$$
$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
