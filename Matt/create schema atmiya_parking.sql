drop schema if exists atmiya_parking;
create schema atmiya_parking;
use atmiya_parking;

create table users (
  uid int auto_increment not null primary key,
  snumber int(8) unsigned unique null,
  password varchar(20) not null,
  firstname varchar(20) not null,
  lastname varchar (20) not null,
  address text not null,
  mobilenumber int (10) not null,
  email varchar(40) unique not null,
  permissions enum('visitor','student','staff') not null default 'visitor'
);

insert into users values (
  null, 10000022, 'password', 'Greg', 'Gregson',
  '50 Gregshouse Lane Beaglehome QLD 5043',
  '0423232123','greg.gregson@atmiya.edu.au', 'student'
);

insert into users values (
  null, 22333423, 'password', 'John', 'Adams',
    '29/374 Johnshouse Road Janestown QLD 5138',
  '0434643753','john.adams@atmiya.edu.au', 'staff'
);

insert into users values (
  null, null, 'password', 'Jess', 'Willis',
    '75 Bluebell Lane Amberley QLD 5034',
  '0454355433','jess.willis@gmail.com', 'visitor'
);

create table permits (
  pid int auto_increment not null primary key,
  uid int not null,
  startdate datetime not null,
  enddate datetime not null,
  liscencenumber varchar(12) not null,
  vehicletype enum('2wheel','4wheel','other') not null,
  department enum('law','maths','science','engineering','admin') not null
);

insert into permits values (
  null, 1, '2016-08-12 00:00:00', '2017-08-12 00:00:00', 'G3D-848', '2wheel', 'maths'
);
