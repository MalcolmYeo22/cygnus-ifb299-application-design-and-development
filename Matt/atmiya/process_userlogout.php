
<html>
  <head>
 
    <!-- bootstrap css -->
    <link href="./bootstrap-3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap-3.3.7/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- our css -->
    <link href="atmiya.css" rel="stylesheet">

    <script src="./bootstrap-3.3.7/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <div class="jumbotron"> 

  </head>

  <body>
     <div class="jumbotron">
       <div class="container">
         <?php
           if( isset($_COOKIE['curruid']) )
             setcookie('curruid', '', time() - (100), "/");
           echo "Logged out successfully.";
         ?>
         </p><p><a class="btn btn-primary btn-lg" href="/atmiya/" role="button">&laquo; Back</a></p>
       </div>
     </div>
          </div>
    <?php include_once('footer.php'); ?>
  </body>
</html>