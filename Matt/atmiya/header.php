<!DOCTYPE html>

<?php 
  if(isset($_COOKIE['curruid'])) {
    $curruid = $_COOKIE['curruid'];
    require_once('../connect_mysql.php');
    $curruresult = mysqli_query($con,"select * from users where uid = '$curruid'");
    $currurecord = mysqli_fetch_array($curruresult);
  }
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="./bootstrap-3.3.7/favicon.ico">

    <title>Atmiya College</title>

    <!-- bootstrap css -->
    <link href="./bootstrap-3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap-3.3.7/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- our css -->
    <link href="atmiya.css" rel="stylesheet">

   <script src="./bootstrap-3.3.7/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/atmiya/">
          <?php if( isset($_COOKIE['curruid'])) {
            echo $currurecord['firstname'].' '.$currurecord['lastname'].' </a>
          </div>
            <div id="navbar" class="navbar-collapse collapse">
              <form class="navbar-form navbar-right" action="process_userlogout.php" method="POST">
                <div class="form-group">
                <button type="submit" id="submit_button" value="login"  class="btn btn-success">Sign out</button>
              </form>';
          }
          else {
          echo 'No User Logged In</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right" action="process_userlogin.php" method="POST">
              <div class="form-group">
              <input type="text" placeholder="Email" class="form-control"  id="user" name="user">
              </div>
              <div class="form-group">
                <input type="password" placeholder="Password" class="form-control"  id="pass" name="pass">
              </div>
              <button type="submit" id="submit_button" value="login"  class="btn btn-success">Sign in</button>
            </form>';
          }?>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>