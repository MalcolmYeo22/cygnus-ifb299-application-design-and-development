<?php include_once('header.php')?>
<?php if( !isset($_COOKIE['curruid']))
      echo '<script type="text/javascript">
           window.location = "./index.php"
      </script>';
?>

  
    <div class="jumbotron">
      <div class="container">
        <h1>Welcome Back, <?php echo $currurecord['firstname']; ?>.</h1>
        <p>Apply or reapply for parking permits at the link below.</p>
        <p><a class="btn btn-primary btn-lg" href="form_permitapplication.php" role="button">Apply for Permit &raquo;</a></p>
      </div>
    </div>

    <div class="container"> <!-- staff items -->

      
          <?php if ( $currurecord['permissions'] == 'staff' ) 
           include_once('staff_dashboard.html'); ?>



      </div><!-- end of container staff items  -->

    <div class="container"> <!-- personal items -->
       <h3>Personal Dashboard</h3> 
      <div class="row"> 

        <div class="col-md-4">
          <h2>Violations</h2>
          <p>If you are looking to pay an outstanding fine or would like to look at your violations please click below.</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2>Frequently Asked Questions</h2>
          <p>Do you have any questions about campus parking policy? Please look at our FAQ section.</p>
          <p><a class="btn btn-default" href="faq.php" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-4">
            <h2>Update User Details</h2>
            <p>Click link to change or update user details.</p>
            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
          </div>
       <div class="col-md-4">
       
       </div>      <!-- end of row -->
    </div> <!-- end of container  personal items -->
  

      </div><!-- end of jumbotron -->
      
      <?php include_once('footer.php')?>