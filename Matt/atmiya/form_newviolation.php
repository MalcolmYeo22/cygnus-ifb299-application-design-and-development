<?php include_once('header.php')?>
            <section class="container">
            <div class="container-page">        
          <div class="col-md-6">
            <!-- Name -->
        <div class="form-group col-lg-12">
            <br>
            <label>First Name</label>
            <input type="" name="" class="form-control" id="" value="" placeholder="eg. John"> 
        </div>
        <div class="form-group col-lg-12">
            <label>Surname Name</label>
            <input type="" name="" class="form-control" id="" value="" placeholder="eg. Adams">
        </div>
            <!--Email -->
        <div class="form-group col-lg-12">
            <label>Email</label>
            <input type="" name="" class="form-control" id="" value="" placeholder="Email Address">
        </div>
            <!-- Contact Information -->
        <div class="form-group col-lg-12">
            <label>Mobile Number</label>
            <input type="" name="" class="form-control" id="" value="" placeholder="Mobile Number">
        </div>
         <div class="form-group col-lg-12">
            <label>Home Number</label>
            <input type="" name="" class="form-control" id="" value="" placeholder="Home Number">
        </div>
         <div class="form-group col-lg-12">
            <label>Work Number</label>
            <input type="" name="" class="form-control" id="" value="" placeholder="Work Number">
        </div>
        <!-- Address -->
        <h2>Home Address</h2>
        <div class="form-group col-lg-12">
            <label>Unit / Street Number</label>
            <input type="" name="" class="form-control" id="" value="" placeholder="eg. Unit 2">
        </div>
        <div class="form-group col-lg-12">
            <label>Street Name</label>
            <input type="" name="" class="form-control" id="" value="" placeholder="eg. Sterling Avenue">
        </div>
        <div class="form-group col-lg-12">
            <label>Suburb</label>
            <input type="" name="" class="form-control" id="" value="" placeholder="eg. Carindale">
        </div>

          <div class="container">
         <div class="form-group">
              <label for="exampleSelect1">State</label>
              <select class="form-control" id="exampleSelect1">
                <option>QLD</option>
                <option>ACT</option>
                <option>WA</option>
                <option>VIC</option>
                <option>TAS</option>
                <option>NSW</option>
              </select>
          </div></div>

        <div class="form-group col-lg-12">
            <label>Postcode</label>
            <input type="" name="" class="form-control" id="" value="" placeholder="eg. 4000">
        </div>

        <!--Vehicle Info -->
        <h2>Vehicle Information</h2>

        <br><div class="container">
          <div class="form-group">
              <label for="exampleSelect2">Vehicle Type</label>
              <select class="form-control" id="exampleSelect1">
                <option>Two Wheeler</option>
                <option>Four Wheeler</option>
              </select>
          </div></div>

        <div class="form-group col-lg-12">
            <label>If Other Please Specify</label>
            <input type="" name="" class="form-control" id="" value="" placeholder="Other">
        </div>

        <h2>Permit Type</h2>

        <div class="container">
        <div class="btn-group" role="group" aria-label="...">
            <button type="button" class="btn btn-default">Daily</button>
            <button type="button" class="btn btn-default">Hourly</button>
        </div>

      <!-- End of Form -->
    
      <div class="col-md-6">
        <h3 class="dark-grey">Terms and Conditions</h3>
        <p>
          By clicking you agree to The Company's' Terms and Conditions
        </p>
        <p>
          While rare, prices are subject to change based on exchange rate fluctuations - 
          should such a fluctuation happen, we may request an additional payment. You have the option to request a full refund or to pay the new price. (Paragraph 13.5.8)
        </p>
        <p>
          Should there be an error in the description or pricing of a product, we will provide you with a full refund (Paragraph 13.5.6)
        </p>
        <p>
          Acceptance of an order by us is dependent on our suppliers ability to provide the product. (Paragraph 13.5.6)
        <p><br><a class="btn btn-default" href="16_PermitRegistrationApproved.html" role="button">Submit Registration Form &raquo;</a></p>
        </p>
      </div>
    </div>
  </section>
<?php include_once('footer.php')?>
