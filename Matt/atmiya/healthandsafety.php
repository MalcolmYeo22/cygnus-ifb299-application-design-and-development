<?php include_once('header.php')?>
    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12">
            <br><p><a class="btn btn-default" href="01_HomePage.html" role="button">Home &raquo;</a></p>
            <div class="container">
          <h2>Health and Safety Issues</h2>
                <br><table style="width:100%">
                      <tr>
                        <th>Type</th>
                        <th>First Name</th>
                        <th>Surname</th>
                        <th>Issue</th>
                        <th>Status</th>
                        <th>Edit</th>
                      </tr>
                      <tr>
                        <td>Student</td>
                        <td>John</td>
                        <td>Adams</td>
                        <td>Smoking Violation</td>
                        <td>Violation Pending</td>
                        <td><a class="btn btn-default" href="" role="button">Edit &raquo;</a></td>
                      </tr>
                      <tr>
                        <td>Casual</td>
                        <td>Amy</td>
                        <td>Dunn</td>
                        <td>Smoking Violations</td>
                        <td>Violation Pending</td>
                        <td><a class="btn btn-default" href="" role="button">Edit &raquo;</a></td>
                      </tr>
                    </table>

              </div>

              <br><p><a class="btn btn-default" href="LoginDashboard_Admin_Students.html" role="button">Back &raquo;</a></p>
<?php include_once('footer.php')?>