<?php include_once('header.php')?>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <br>
          <p><a class="btn btn-default" href="/atmiya" role="button">&laquo; Home</a></p>
        </div>
      </div>
            <div class="container">

    <div class="panel-group" id="accordion">
        <div class="faqHeader"><h2>Frequently Asked Questions</div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">I am a student or staff member, what permits am I eligible for?
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    As a student or staff member, you are eligible for an hourly, daily, monthly or yearly parking permit.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">I am a casual visitor to the campus, what permits am I eligible for?
                    </a>
                </h4>
            </div>
            <div id="collapseTen" class="panel-collapse collapse">
                <div class="panel-body">
                    As a casual visitor to the campus, you are eligible for an hourly or daily parking permit.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">What are the costs of a permit? Are these different for staff, students or visitors?</a>
                </h4>
            </div>
            <div id="collapseEleven" class="panel-collapse collapse">
                <div class="panel-body">
                    Regardless of position amongst the campus, the prices for parking are as follows:

                    <br>Hourly - $2/hour
                    <br>Daily - $10/day
                    <br>Monthly - $100/month
                    <br>Yearly - $500/year
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve">Do I have to register as a user prior to applying for a parking permit?</a>
                </h4>
            </div>
            <div id="collapseTwelve" class="panel-collapse collapse">
                <div class="panel-body">Yes. This will allow you to re-new forms, balance any outstanding payments, update your details, and contact us about any queries regarding parking.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen">How will I know if my permit application has been successful?</a>
                </h4>
            </div>
            <div id="collapseThirteen" class="panel-collapse collapse">
                <div class="panel-body">You will receive a confirmation email upon your application being successful.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen">When will I receive my permit?</a>
                </h4>
            </div>
            <div id="collapseFourteen" class="panel-collapse collapse">
                <div class="panel-body">Once full payment has been received successfully, you will receive the parking permit via email to be printed out.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen">Where do I display my permit?</a>
                </h4>
            </div>
            <div id="collapseFifteen" class="panel-collapse collapse">
                <div class="panel-body">For four-wheeled vehicles, the permit is to be affixed to the vehicle’s windshield at the lower corner of the driver side. For two-wheeled vehicles, the permit is to be affixed to front of the vehicle.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSixteen">What information is displayed on my parking permit?</a>
                </h4>
            </div>
            <div id="collapseSixteen" class="panel-collapse collapse">
                <div class="panel-body">On your parking permit, the details of the vehicle the permit is assigned to (make, model, year, colour, registration number) will be displayed along with permit expiry date and a unique identification number assigned to the permit.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeventeen">Am I guaranteed a parking space?</a>
                </h4>
            </div>
            <div id="collapseSeventeen" class="panel-collapse collapse">
                <div class="panel-body">No. Unfortunately there is only a specific amount of parking spaces available. However, most of the time it is highly likely you will receive a parking space.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEighteen">How for I pay for my parking permit?</a>
                </h4>
            </div>
            <div id="collapseEighteen" class="panel-collapse collapse">
                <div class="panel-body">You may either pay for the parking permit online after application, over the phone or at the administration building on campus.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNineteen">Can I be the only person on the permit?</a>
                </h4>
            </div>
            <div id="collapseNineteen" class="panel-collapse collapse">
                <div class="panel-body">No. Anybody can use the permit as long as it is only associated with the car that it is registered to.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwenty">What are the fines for parking violations?</a>
                </h4>
            </div>
            <div id="collapseTwenty" class="panel-collapse collapse">
                <div class="panel-body">Fines for minor offenses may be up to $100
                <br>Fines for major offenses may be up to $200
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyOne">How do I pay for a parking fine?</a>
                </h4>
            </div>
            <div id="collapseTwentyOne" class="panel-collapse collapse">
                <div class="panel-body">You may either pay for a parking fine via logging in to your account/dashboard, paying over the phone, or visiting the administration building on campus.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyTwo">Which vehicles are eligible for a parking permit?</a>
                </h4>
            </div>
            <div id="collapseTwentyTwo" class="panel-collapse collapse">
                <div class="panel-body">All basic two-wheeled or four-wheeled vehicles are eligible for a parking permit. Trucks, 
                Vehicles that you believe are not categorised into either basic two-wheeled or four-wheeled vehicles can be noted as ‘other’ upon permit application but may require evidence to support application to ensure eligibility is met. Contact the university help centre for further assistance.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyThree">If I lose or misplace my permit, how can I obtain a replacement?</a>
                </h4>
            </div>
            <div id="collapseTwentyThree" class="panel-collapse collapse">
                <div class="panel-body">If you lose or misplace a parking permit, you may request a replacement by either logging into your account/dashboard and following the links or contact the university directly by phoning or visiting the administration building.
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyFour">If I change vehicles, do I require a new permit?</a>
                </h4>
            </div>
            <div id="collapseTwentyFour" class="panel-collapse collapse">
                <div class="panel-body">You do not require a new permit but you will need to update your permit details online and re-affix the updated permit to your vehicle. The previous permit will then be cancelled.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwentyFive">Are disabled car parks available?</a>
                </h4>
            </div>
            <div id="collapseTwentyFive" class="panel-collapse collapse">
                <div class="panel-body">Yes. Disabled car parks are available to persons who have a government issued disabled parking affixed to their vehicle. This must be displayed in order to use a disabled parking permit.
                </div>
            </div>
        </div>

         <br>
          <p><a class="btn btn-default" href="/atmiya" role="button">&laquo; Home</a></p>
        </div>
    </div>
</div>

      
      <?php include_once('footer.php')?>
