
<?php

 include_once('header.php');
  require_once('../connect_mysql.php');
  
  $result = mysqli_query($con,"select * from users")
    or die("failed to get data from user table: ".mysqli_error($con));

    echo '<h1>USERS</h1><table><tr>
        <th>user id</th>
        <th>student number</th>
        <th>password</th>
        <th>first name</th>
        <th>last name</th>
        <th>address</th>
        <th>mobile</th>
        <th>email</th>
        <th>permissions</th>
    </b></tr>';
    
    while ($record = mysqli_fetch_array($result)) {
      echo '<tr>
        <td>' . $record['uid'] . '</td>
        <td>' . $record['snumber'] . '</td>
        <td>' . $record['password'] . '</td>
        <td>' . $record['firstname'] . '</td>
        <td>' . $record['lastname'] . '</td>
        <td>' . $record['address'] . '</td>
        <td>' . $record['mobilenumber'] . '</td>
        <td>' . $record['email'] . '</td>
        <td>' . $record['permissions'] . '</td>
    </tr></b>';
    }
    
    echo '</table>';
    
      $result = mysqli_query($con,"select * from permits")
    or die("failed to get data from user table: ".mysqli_error($con));

    echo '<h1>PERMITS</h1><table><tr>
        <th>permit id</th>
        <th>user id</th>
        <th>start date</th>
        <th>end date</th>
        <th>liscence number</th>
        <th>vehicle type</th>
        <th>department</th>
    </b></tr>';
    
    while ($record = mysqli_fetch_array($result)) {
      echo '<tr>
        <td>' . $record['pid'] . '</td>
        <td>' . $record['uid'] . '</td>
        <td>' . $record['startdate'] . '</td>
        <td>' . $record['enddate'] . '</td>
        <td>' . $record['liscencenumber'] . '</td>
        <td>' . $record['vehicletype'] . '</td>
        <td>' . $record['department'] . '</td>
    </tr></b>';
    }
    
    echo '</table>';
    mysqli_close($con);
    
    include_once('footer.php');
?>