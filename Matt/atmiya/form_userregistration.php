<?php include_once('header.php')?>


<div class="container">
 <br>
 <p><a class="btn btn-default" href="./index.php" role="button"> &laquo; Home</a></p>
<p>
<h2>User Registration</h2>
<p>Please fill out the form provided. Fields with a * are mandatory. <br> If you need help or have questions please contact our Help desk: <b>000-111-222-333</b> <br>or email: <b>helpdesk@atmiyacollege.edu.au</b></p>
           
</p>

<form action="./process_userregistration.php" method="post">

<p>


<br>First Name*: <br>
<input  class="btn btn-default"  type = "text" name = "firstname" placeholder = 'eg. John' />

<br>Last Name*: <br>
<input  class="btn btn-default"  type = "text" name = "lastname" placeholder = 'eg. Adams' />
<br>

<br>Student Number/ Staff Number: <br> 

<input  class="btn btn-default"  type = "text" name = "snumber" size = "8" />
<br>
<small>This field is not required, however if you provide a valid student or staff number you will be able to apply for monthly and yearly permits. Without one you will only be able to apply for hourly or daily permits.</small>
<br>

<br>Email address*: <br> 
<input  class="btn btn-default"  type = "text" name = "email" />

<br>Password*: <br>
<input  class="btn btn-default"  type = "text" name = "password" />
<br>
<small>Please note that the email and password you provide here are your login credentials, and that any permit you apply for will be sent to the listed email address.</small>
<br>

<br>Address*: <br>
<input  class="btn btn-default"  style="width: 80%" type = "text" name = "address" />

<br>Mobile Number*: <br>
<input  class="btn btn-default"  type = "text" name = "mobile" size = "10" />
<br>
</p>
<p>
<input class="btn btn-primary btn-lg"  type="submit" name="submit" value="Submit Registration Form &raquo;" />
</p>

</form>
</div class>


<?php include_once('footer.php')?>