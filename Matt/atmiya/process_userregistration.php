
<?php
  $herefromform = isset($_POST['submit']);
  if ($herefromform) { 

    $snumber = $_POST['snumber'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $firstname = $_POST['firstname'];   
    $lastname = $_POST['lastname'];
    $address = $_POST['address'];
    $mobile = $_POST['mobile'];
    
    $missing_required = array();
    if (empty($email)) $missing_required[] =  'email address';
    if (empty($password)) $missing_required[] = 'password';
    if (empty($_POST['firstname'])) $missing_required[] = 'first name';
    if (empty($_POST['lastname'])) $missing_required[] = 'last name';
    if (empty($_POST['address'])) $missing_required[] = 'address';    
    if (empty($_POST['mobile'])) $missing_required[] = 'mobile number';
    
    require_once('../connect_mysql.php');
    
    $invalid_data = array();
    
    if (empty($snumber)) $permissions = 'visitor';
    else {
      if (!is_numeric($snumber)) $invalid_data[] = 'student/staff number: must have numeric input. <br>';
      if ((int)$snumber < 1000000 || (int)$snumber > 29999999 ) $invalid_data[] = 'student/staff number: given index is not a valid student or staff number. <br>';   
      $result = mysqli_query($con,"select * from users where snumber = \"$snumber\"");
      if($result && mysqli_num_rows($result)) 
        $invalid_data[] = 'student/staff number: a user with this student/staff number already exists in the database. <br>';
      
      if (empty($invalid_data)) {
        $permissions = (int)$snumber < 20000000 ? "student": "staff";
      }
    }
    
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) $invalid_data[] = 'email address: a valid email will be of the form "user@example.com". <br>';
    $result = mysqli_query($con,"SELECT * FROM users WHERE email = \"$email\"");
    if($result && mysqli_num_rows($result)) $invalid_data[] = 'email address: a user with this email already exists in the database. <br>';
    
    if (!is_numeric($mobile)) $invalid_data[] = 'mobile number: must have numeric input. <br>';
  } 
   
?>

<?php include_once('header.php')?>

     <div class="jumbotron">
      <div class="container">
            <p><a class="btn btn-default" href="./index.php" role="button"> &laquo; Home</a></p>
            <br>
        <?php 
        if (!$herefromform) {
          echo 
          '<p>No data submitted. Please go to the user registration form to register as a new user. </p>
           <br>
           <p><a class="btn btn-primary btn-lg" href="./form_userregistration.php" role="button"> &laquo; Register a new user</a></p>';
         }
         else if (!empty($missing_required)) {
           echo '<p>You must enter the following data: ';
           $count = count($missing_required);
           for($i = 0; $i < $count-1; $i++) {
             echo "$missing_required[$i], ";
           }
           if ($count > 1) echo "and ";
           echo $missing_required[$count-1].'.</p>';
         }
         else if (!empty($invalid_data)) {
           echo '<p>The following entries are invalid: <br>';
           foreach($invalid_data as $data) echo $data;
           echo "</p>";

         }
         else {
           echo "your snumber is ".$snumber."
           <br> your email is ".$email."
           <br> your password is ".$password."
           <br> your firstname is ".$firstname."
           <br> your lastname is ".$lastname."
           <br> your address is ".$address."
           <br> your mobile is ".$mobile."
           <br> your permissions is ".$permissions."<br>";
           
           $query = '';
           
           if (empty($snumber)) {
             $query = 'insert into users (uid, snumber, password, firstname, 
             lastname, address, mobilenumber, email, permissions) values (NULL, NULL, ?, ?, ?, ?, ?, ?, ?)';
             $prepared_stmt = mysqli_prepare($con, $query); 
             mysqli_stmt_bind_param($prepared_stmt,"issssiss",$password,$firstname,$lastname,$address,$mobile,$email,$permissions);
             //here "issssiss" is the datatypes: (int, string, string...)
            
          }
          
          else  {
           $query = 'insert into users (uid, snumber, password, firstname, 
           lastname, address, mobilenumber, email, permissions) values (NULL, ?, ?, ?, ?, ?, ?, ?, ?)';
           $prepared_stmt = mysqli_prepare($con, $query); 
           mysqli_stmt_bind_param($prepared_stmt,"issssiss",$snumber,$password,$firstname,$lastname,$address,$mobile,$email,$permissions);
           //here "ssssiss" is the datatypes: (int, string, string...)
          }
           
           echo "tried query: '".query."'<br>";
           
           mysqli_stmt_execute($prepared_stmt);
           $affected_rows = mysqli_stmt_affected_rows($prepared_stmt);
           if($affected_rows == 1){
            echo 'Registration completed correctly.';
           }
           else{
            echo 'Database error; couldnt create new user.';
           }
           mysqli_stmt_close($prepared_stmt);

         }
         mysqli_close($con);
         ?>

      </div>
      </div>
      
 <?php include_once('footer.php')?>
 
 